package com.gitlab.katsella.schedulerapiclient.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "scheduler-api")
public class SchedulerApiConfig {
    public String url;
    public String jobDoneEndpoint = "/api/v1/jobStatus/done";

    public String getJobDoneUrl() {
        return url + jobDoneEndpoint;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setJobDoneEndpoint(String jobDoneEndpoint) {
        this.jobDoneEndpoint = jobDoneEndpoint;
    }
}
