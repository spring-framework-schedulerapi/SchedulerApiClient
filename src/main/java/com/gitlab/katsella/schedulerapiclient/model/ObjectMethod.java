package com.gitlab.katsella.schedulerapiclient.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ObjectMethod {
    private Object instance;
    private Class<? extends Object> clazz;
    private Method method;

    public ObjectMethod(Class<? extends Object> clazz, Object instance, Method method) {
        this.clazz = clazz;
        this.instance = instance;
        this.method = method;
    }

    public Object getInstance() {
        return instance;
    }

    public Method getMethod() {
        return method;
    }

    public Class<? extends Object> getClazz() {
        return clazz;
    }

    public <T> T invoke(Object... args) throws IllegalAccessException {
        try {
            return (T) method.invoke(instance, args);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e.getTargetException());
        }
    }
}
