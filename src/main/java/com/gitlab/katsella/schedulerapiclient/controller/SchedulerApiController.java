package com.gitlab.katsella.schedulerapiclient.controller;

import com.gitlab.katsella.schedulerapiclient.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiclient.service.SchedulerApiService;
import com.gitlab.katsella.schedulerapiclient.util.VersionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.SpringVersion;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPatternParser;

import java.lang.reflect.InvocationTargetException;

@RestController
public class SchedulerApiController {

    private final RequestMappingHandlerMapping requestMappingHandlerMapping;

    private SchedulerApiService schedulerApiService;

    @Autowired
    public SchedulerApiController(RequestMappingHandlerMapping requestMappingHandlerMapping) {
        this.requestMappingHandlerMapping = requestMappingHandlerMapping;
    }

    public void registerEndpoint(String endpoint, SchedulerApiService schedulerApiService) {
        this.schedulerApiService = schedulerApiService;
        var mappingInfoBuilder = RequestMappingInfo
                .paths(endpoint)
                .methods(RequestMethod.POST);

        boolean isVersionBefore = VersionUtils.isVersionBefore(SpringVersion.getVersion(), "5.3.13");
        if (!isVersionBefore) {
            RequestMappingInfo.BuilderConfiguration options = new RequestMappingInfo.BuilderConfiguration();
            options.setPatternParser(new PathPatternParser());
            mappingInfoBuilder.options(options);
        }

        RequestMappingInfo mappingInfo = mappingInfoBuilder.build();
        try {
            requestMappingHandlerMapping.registerMapping(mappingInfo, this, this.getClass().getDeclaredMethod("schedulerTrigger", SchedulerApiBody.class));
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Probably bug. 'schedulerTrigger(SheduelerApiBody)' method not found");
        }
    }

    /*
        Method name should be same with registerEnpoint() method name
        Method type is POST, defaulth enpoint is /api/v1/scheduler
     */
    public void schedulerTrigger(
            @RequestBody SchedulerApiBody body
    ) throws InvocationTargetException, IllegalAccessException {
        schedulerApiService.triggerSchedulerJob(body);
    }

}
