package com.gitlab.katsella.schedulerapiclient.util;

import java.security.InvalidParameterException;

public class VersionUtils {
    public static boolean isVersionBefore(String v1, String v2) {
        String[] splits1 = v1.split("\\.");
        String[] splits2 = v2.split("\\.");

        if (splits1.length != 3 || splits2.length != 3)
            throw new InvalidParameterException("Version length not 3. v1: " + v1 + " v2: " + v2);

        for (int i = 0; i < splits1.length; i++) {
            int num1 = Integer.parseInt(splits1[i]);
            int num2 = Integer.parseInt(splits2[i]);
            if (num1 < num2)
                return true;
        }
        return false;
    }


}
