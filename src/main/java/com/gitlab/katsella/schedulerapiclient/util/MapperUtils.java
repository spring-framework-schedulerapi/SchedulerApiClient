package com.gitlab.katsella.schedulerapiclient.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MapperUtils {

    private static final Logger LOGGER = Logger.getLogger(MapperUtils.class.getName());

    private MapperUtils() {
    }

    public static final DateFormat yyyyMMddHHmmssSSSZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private static final ObjectMapper objectMapper;
    private static final ObjectMapper objectMapperDateFormatted;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.registerModule(new JavaTimeModule());

        objectMapperDateFormatted = new ObjectMapper();
        objectMapperDateFormatted.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapperDateFormatted.registerModule(new JavaTimeModule());
        objectMapperDateFormatted.setDateFormat(yyyyMMddHHmmssSSSZ);
    }

    public static <T> T readValue(String value, Class<T> valueClass) throws IOException {
        try {
            if (StringUtils.isBlank(value))
                return null;
            return objectMapper.readValue(value, valueClass);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static String writeValueAsString(Object object) throws JsonProcessingException {
        if (Objects.isNull(object)) return null;
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static String writeValueAsStringDateFormattedWithoutException(Object object) {
        try {
            return writeValueAsStringDateFormatted(object);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE, "writeValueAsStringDateFormattedWithoutException:: error on parsing json object", e);
            return null;
        }
    }

    public static String writeValueAsStringDateFormatted(Object object) throws JsonProcessingException {
        if (Objects.isNull(object)) return null;
        return objectMapperDateFormatted.writeValueAsString(object);
    }

}
