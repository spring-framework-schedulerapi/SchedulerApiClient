package com.gitlab.katsella.schedulerapiclient.exception;

public class SchedulerApiException extends RuntimeException {
    public SchedulerApiException(String msg) {
        super(msg);
    }
}
