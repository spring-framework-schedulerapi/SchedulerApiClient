package com.gitlab.katsella.schedulerapiclient.processor.Request.impl;

import com.gitlab.katsella.schedulerapiclient.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiclient.processor.Request.model.RequestModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateRequestProcessor implements RequestProcessor {

    private RestTemplate restTemplate;

    public RestTemplateRequestProcessor(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestTemplateRequestProcessor() {
        restTemplate = new RestTemplate();
    }

    @Override
    public <T> T post(RequestModel requestModel, Class<T> tClass) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("email", "first.last@example.com");

            HttpEntity request = new HttpEntity(requestModel.getBody());

            ResponseEntity<T> response = restTemplate.postForEntity(requestModel.getUrl(), request, tClass);
            return response.getBody();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
