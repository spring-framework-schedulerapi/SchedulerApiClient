package com.gitlab.katsella.schedulerapiclient.processor;

import com.gitlab.katsella.schedulerapiclient.model.ObjectMethod;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AnnotationFinder implements ApplicationContextAware {

    private final List<Class<? extends Annotation>> springTypeAnnotationsClass = List.of(Service.class, Controller.class, Configuration.class);

    private ApplicationContext appContext;

    List<Class<?>> classList;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appContext = applicationContext;
    }

    private List<String> getBeansNames() {
        Set<String> list = new LinkedHashSet<>();
        for (Class<? extends Annotation> annotation : springTypeAnnotationsClass) {
            String[] array = appContext.getBeanNamesForAnnotation(annotation);
            list.addAll(Arrays.stream(array).collect(Collectors.toList()));
        }
        return list.stream().collect(Collectors.toList());
    }

    private void load() {
        try {
            if (Objects.isNull(classList)) {
                List<String> beansNames = getBeansNames();
                classList = beansNames.stream().map(e -> ClassUtils.getUserClass(appContext.getBean(e))).collect(Collectors.toList());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<Class<?>> getUserClass() {
        load();
        return classList;
    }

    public List<Class<?>> getAnnotationClass(Class<? extends Annotation> annotationClass) {
        return getUserClass().stream().filter(e -> e.isAnnotationPresent(annotationClass)).collect(Collectors.toList());
    }

    public List<ObjectMethod> findAnnotatedMethod(Class<? extends Annotation> annotationClass) {
        List<ObjectMethod> methods = new ArrayList<>();
        for (Class<?> clazz : getUserClass()) {
            methods.addAll(findClassAnnotatedMethod(clazz, annotationClass));
        }
        return methods;
    }

    private List<ObjectMethod> findClassAnnotatedMethod(Class<?> clazz, Class<? extends
            Annotation> annotationClass) {
        Method[] methods = clazz.getMethods();
        List<ObjectMethod> annotatedMethods = new ArrayList<>(methods.length);
        for (Method method : methods) {
            if (method.isAnnotationPresent(annotationClass)) {
                Object instance = appContext.getBean(clazz);
                if (Objects.isNull(instance))
                    throw new NullPointerException("Method object instance not found");
                ObjectMethod objectMethod = new ObjectMethod(clazz, instance, method);
                annotatedMethods.add(objectMethod);
            }
        }
        return annotatedMethods;
    }

}
