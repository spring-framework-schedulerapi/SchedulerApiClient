package com.gitlab.katsella.schedulerapiclient.processor.Request;

import com.gitlab.katsella.schedulerapiclient.processor.Request.model.RequestModel;

public interface RequestProcessor {

    <T> T post(RequestModel requestModel, Class<T> tClass);

}
