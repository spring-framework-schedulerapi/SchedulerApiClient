package com.gitlab.katsella.schedulerapiclient.processor;

import com.gitlab.katsella.schedulerapiclient.model.SchedulerApiBody;

public interface ISchedulerApiJobDone {
    void jobDone(SchedulerApiBody body);
}
