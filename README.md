# Spring Framework SchedulerApiClient

Listens for rest request and execute requested method.

## Features
Basicly this package allows you to trigger scheduled job thru api call.

| Basic schema |
| ------ |
|![schema](/uploads/e1e3b495601a2602d9ca09fac46c81bd/schema.png)|


## Installing
Add this dependency <br><br>
[![Maven Central](https://img.shields.io/maven-central/v/com.gitlab.katsella/schedulerapiclient.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.gitlab.katsella%22%20AND%20a:%22schedulerapiclient%22)
```
<dependency>
    <groupId>com.gitlab.katsella</groupId>
    <artifactId>schedulerapiclient</artifactId>
    <version>1.0.1-RC3</version>
</dependency>
```

## Usage

## SchedulerApiClient configuration
```java
@EnableSchedulerApi // <-- Add this
@SpringBootApplication
public class SchedulerApiClient {
    public static void main(String[] args) {
        SpringApplication.run(SchedulerApiClient.class, args);
    }
}
```
```java
@Service
public class ScheduledTester {
    @SchedulerApi(name = "testMethod") // defaulth name "{className}.{methodName}" in this case "ScheduledTester.scheduled", set this name to SchedulerApiServer configuration.
    public void scheduled(){
        ...
    }
}
```
```java
// Add this to your properties file
scheduler-api.url=http://localhost:9710  // SchedulerApiServer url
```

### If you need to set Authentication header
```java
public class MyRequestProcessor extends RestTemplateRequestProcessor {
    public MyRequestProcessor() {
        super(); // you can pass RestTemplate as parameter if needed
    }

    @Override
    public <T> T post(RequestModel requestModel, Class<T> tClass) {
        Map<String, String> headers = requestModel.getHeaders();
        if (Objects.isNull(headers))
            headers = new HashMap<>();
        headers.put("Authentication", "Bearer token=");
        return super.post(requestModel, tClass);
    }
}

@Component
public class RequestProcessorConfig {
    @Bean
    public RequestProcessor requestProcessor() {
        return new MyRequestProcessor();
    }
}
```
For SchedulerApiServer check out this -> https://gitlab.com/katsella/SchedulerApiServer
